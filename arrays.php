<?php
$ufs = ['ES', 'MG', 'RJ', 'SP'];
$ufsNames = ['São Paulo', 'Rio de Janeiro', 'Minas Gerais', 'Espírito Santo'];

sort($ufs);
sort($ufsNames);

for ($ufIndex = 0; $ufIndex < count($ufs); $ufIndex++) {
    $finalUfs[$ufs[$ufIndex]] = $ufsNames[$ufIndex];
}

foreach($finalUfs as $ufKey=>$ufValue) {
    echo "{$ufKey} - {$ufValue}\n";
}

?>
