<?php
class Foo {
	private $a;
	private $b;
	private $c;

	public function getA () {
		return $this->a;
	}
	public function setA ($a) {
		$this->a = $a;
	}

	public function getB () {
		return $this->b;
	}
	public function setB ($b) {
		$this->b = $b;
	}

	public function getC () {
		return $this->c;
	}
	public function setC ($c) {
		$this->c = $c;
	}

	public function multiplyAll() {
		return $this->getA() * $this->getB() * $this->getC();
	}
}

$foo = new Foo;
$foo->setA(2);
$foo->setB(3);
$foo->setC(4);
echo $foo->multiplyAll();

?>
