<?php

namespace ProvaIN8\Http\Controllers;

use Illuminate\Http\Request;

class PersonsController extends Controller
{
    public function index()
    {
        $persons = Persons::get();
        return response()->json($persons);
	}

	public function store(Request $request)
    {
        $person = new Person();
        $person->fill($request->all());
        $person->save();

        return response()->json($person, 201);
	}

	public function update(Request $request, $id)
    {
        $person = Person::find($id);

        if(!$person) {
            return response()->json([
                'message' => 'Record not found',
            ], 404);
        }

        $person->fill($request->all());
        $person->save();

        return response()->json($person);
	}

	public function destroy($id)
    {
        $person = Person::find($id);

        if(!$person) {
            return response()->json([
                'message'   => 'Record not found',
            ], 404);
        }

        $person->delete();
    }
}
