<?php

namespace ProvaIN8;

use Illuminate\Database\Eloquent\Model;

class Person extends Model
{
    protected $fillable = ['name', 'email', 'birthdate', 'cellphone'];
    protected $dates = ['deleted_at'];
}
