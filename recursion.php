<?php
function findSmaller($next = 0) {
	if ($next % 2 != 0) {
		return findSmaller($next + 1);
	}

	if ($next % 3 != 0) {
		return findSmaller($next + 2);
	}

	if ($next % 10 != 0) {
		return findSmaller($next + 3);
	}

	return $next;
}

findSmaller();
?>
