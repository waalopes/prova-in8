import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";

import Header from "./components/Header";
import Register from "./components/Register";
import List from "./components/List";
import About from "./components/About";
import Footer from "./components/Footer";

function App() {
  return (
    <Router>
      <Header />
      <Switch>
        <Route path="/register">
          <Register />
        </Route>
        <Route path="/list">
          <List />
        </Route>
        <Route path="/about">
          <About />
        </Route>
        <Route path="/">
          <Register />
        </Route>
      </Switch>
      <Footer />
    </Router>
  );
}

export default App;
